package com.wemirr.framework.security.configuration.server.third.converts;


import com.wemirr.framework.security.configuration.server.third.Oauth2UserConverter;
import com.wemirr.framework.security.constant.SecurityConstants;
import com.wemirr.framework.security.domain.OAuth2ThirdAccount;
import lombok.RequiredArgsConstructor;
import org.springframework.security.oauth2.core.user.OAuth2User;
import org.springframework.stereotype.Component;

/**
 * 转换通过Github登录的用户信息
 *
 * @author vains
 */
@RequiredArgsConstructor
@Component(SecurityConstants.THIRD_LOGIN_GITHUB)
public class GithubUserConverter implements Oauth2UserConverter {

    private final GiteeUserConverter userConverter;

    protected static final String LOGIN_TYPE = SecurityConstants.THIRD_LOGIN_GITHUB;

    @Override
    public OAuth2ThirdAccount convert(OAuth2User oAuth2User) {
        // github与gitee目前所取字段一致，直接调用gitee的解析
        OAuth2ThirdAccount thirdAccount = userConverter.convert(oAuth2User);
        // 提取location
        Object location = oAuth2User.getAttributes().get("location");
        thirdAccount.setLocation(String.valueOf(location));
        // 设置登录类型
        thirdAccount.setType(LOGIN_TYPE);
        return thirdAccount;
    }
}
